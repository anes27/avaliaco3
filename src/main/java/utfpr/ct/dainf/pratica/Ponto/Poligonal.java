package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();
    private int i;
    
    public void insert(T p){
        vertices.add(p);
    }
    
    public int getN(){
         
        return vertices.size();
    }
    
    public T get(int i){
        this.i = i;
        if(i > getN() || i < 0)
            throw new RuntimeException("get("+i+"): indice invalido");
        else 
            return vertices.get(i);
    }
    
    public void set(int i, T p){
        if(i < 0 || i > getN())
            throw new RuntimeException("set("+i+"): indice invalido");
        else 
            vertices.set(i, p);
    }
    
    public double getComprimento(){
        double comprimento = 0;
        int j;

            for(j = 0; j < getN()-1; j++){
                comprimento += get(j).dist(get(j+1));
            }
        return comprimento;
    }    
}
