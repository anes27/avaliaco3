/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1956477
 */
public class PoligonalFechada extends Poligonal{
    
  @Override
public double getComprimento(){
        double comprimento = 0;
        int j;

            for(j = 0; j < getN()-1; j++){
                comprimento += get(j).dist(get(j+1));
            }
        return comprimento;
    }    
}